//NA 2017/2018: Zadaća 3, Zadatak 1
#include <iostream>
#include <cmath>
#include <vector>
#include <utility>
#include <algorithm>
#include <stdexcept>
#include <limits>



class AbstractInterpolator{
    protected:
    std::vector<std::pair<double,double>> v;
    mutable int pozicija;
    
    public:
    AbstractInterpolator(const std::vector<std::pair<double,double>> &data){
        v=data;
        std::sort(v.begin(),v.end(),[](std::pair<double,double> x, std::pair<double,double> y){
            return x.first<y.first;
        });
        for(int i=0;i<int(v.size())-1;i++){
                if(fabs(v[i].first-v[i+1].first)<pow(10,-10)) throw std::domain_error("Invalid data set");
            }
    }
    virtual double operator()(double x) const{
        return 0;
    }
    int Locate(double x) const{
        if(x<=v[0].first) return 0;
        if(x>v[int(v.size()-1)].first) return int(v.size()); 
        
        auto tmp(std::lower_bound(v.begin(),v.end(),x,[x](std::pair<double,double> lhs,double x)->bool{
            return x>lhs.first;
        }));
        pozicija=tmp-v.begin();
        return pozicija;
    }
};

class LinearInterpolator:public AbstractInterpolator{
    public:
    LinearInterpolator(const std::vector<std::pair<double,double>> &data):AbstractInterpolator(data){}
    double operator()(double x)const override{
        int i(abs(Locate(x)-1)),n(int(v.size()));
        for(int j=0;j<n;j++) if(fabs(x-int(v.size()))<pow(10,-10)) return v.at(j).second;
        if(x<v.at(0).first)
        return (v[1].first-x)*v[0].second/(v[1].first-v[0].first)+(x-v[0].first)*v[1].second/(v[1].first-v[0].first);
        else if(x>v.at(n-1).first)
        return (v[n-1].first-x)*v[n-2].second/(v[n-1].first-v[n-2].first)+(x-v[n-2].first)*v[n-1].second/(v[n-1].first-v[n-2].first);
        return (v[i+1].first-x)*v[i].second/(v[i+1].first-v[i].first)+(x-v[i].first)*v[i+1].second/(v[i+1].first-v[i].first);
    }
};

class PolynomialInterpolator:public AbstractInterpolator{
    private:
    std::vector<double> y,a;
    public:
    PolynomialInterpolator(const std::vector<std::pair<double, double>> &data):AbstractInterpolator(data),a(int(data.size())){
        int n(int(v.size()));
        for(int i=0; i<n; i++){
            a[i]=v[i].second;
        }
        for(int j=1; j<=n-1; j++){
            for(int i=n-1; i>=j; i--){
                if(i==n-1) y.push_back(a[i]);
                a[i]=(a[i]-a[i-1])/(v[i].first-v[i-j].first);
            }
        }
        y.push_back(a[n-1]);
    }
    double operator()(double x) const override{
        int n(int(v.size()));
        double f(a[n-1]);
        for(int i=n-2;i>=0;i--)
            f=f*(x-v[i].first)+a[i];
        return f;
    }
    void AddPoint(const std::pair<double, double> &p){
        int n(int(v.size()));
        for(int i=0;i<n;i++){
            if(fabs(v[i].first-p.first)<pow(10,-10) || fabs(v[i].second-p.second)<pow(10,-10)) throw std::domain_error("Invalid point");
        }
        double tmp(p.second);
        for(int j=0;j<n;j++){
            double pom(tmp);
            tmp=(tmp-y[j])/(p.first-v[n-1-j].first);
            y[j]=pom;
        }
        y.push_back(tmp);
        v.push_back(p);
        a.push_back(tmp);
    }
    std::vector<double> GetCoefficients() const{
        int n(int(v.size()));
        std::vector<double> koef(n,0);
        std::vector<double> w(n+1,1);
        for(int i=1; i<=n; i++){
            w[i]=w[i-1];
            for(int j=i-1; j>=1; j--)
                w[j]=w[j-1]-v[i-1].first*w[j];
            w[0]=-1*v[i-1].first*w[0];
        }
        double f(1);
        for(int i=1;i<=n;i++){
            f=1;
            for(int j=0; j<n; j++){
                if(j!=i-1)
                    f=f*(v[i-1].first-v[j].first);
            }
            f=v[i-1].second/f;
            std::vector<double> pom(w);
            for(int j=n-1;j>=0;j--){
                pom[j]+=v[i-1].first*pom[j+1];
                koef[j]+=f*pom[j+1];
            }
        }
        return koef;
    }
};

class PiecewisePolynomialInterpolator:public AbstractInterpolator{
    private:
    int red;
    public:
    PiecewisePolynomialInterpolator(const std::vector<std::pair<double, double>> &data, int order):AbstractInterpolator(data),red(order){
        if(red<1 || red>=v.size()) throw std::domain_error("Invalid order");
        
    }
    double operator()(double x) const override{
        int i(abs(Locate(x))),n(int(v.size())),donja_gr,gornja_gr,pocetak,kraj; 
        if(red%2==0){
            donja_gr=i-red/2;
            gornja_gr=i+red/2;
        }
        else{
            donja_gr=i-(red-1)/2;
            gornja_gr=i+(red+1)/2;
        }
        
        if(donja_gr<1){
            pocetak=0;
            kraj=red;
        }
        else if(gornja_gr>n){
            pocetak=n-1-red;
            kraj=n-1;
        }
        else{
            pocetak=donja_gr-1;
            kraj=gornja_gr-1;
        }
        
        double p(0),suma(0);
        for(int j=pocetak; j<=kraj;j++){
            p=v[j].second;
            for(int k=pocetak;k<=kraj;k++){
                if(j!=k) 
                    p*=(x-v[k].first)/(v[j].first-v[k].first);
            }
            suma+=p;
        }
        return suma;
    }

}; 

class SplineInterpolator:public AbstractInterpolator{
    private:
    std::vector<double> alfa,r;
    public:
    SplineInterpolator(const std::vector<std::pair<double, double>> &data):AbstractInterpolator(data),alfa(int(data.size()),0),r(int(data.size()),0) {
        int n(int(v.size()));
        
        r[0]=0; 
        r[n-1]=0;
        for(int i=1;i<n-1;i++){
            alfa[i]=2*(v[i+1].first-v[i-1].first);
            r[i]=3*((v[i+1].second-v[i].second)/(v[i+1].first-v[i].first)-(v[i].second-v[i-1].second)/(v[i].first-v[i-1].first));
        }
        
        double mi;
        for(int i=1;i<n-2;i++){
            mi=(v[i+1].first-v[i].first)/alfa[i];
            alfa[i+1]=alfa[i+1]-mi*(v[i+1].first-v[i].first);
            r[i+1]=r[i+1]-mi*r[i];
        }
        r[n-2]=r[n-2]/alfa[n-2];
        for(int i=n-3;i>=1;i--)
        r[i]=(r[i]-(v[i+1].first-v[i].first)*r[i+1])/alfa[i];
    }
    
    double operator()(double x) const override{
        int i(abs(Locate(x))),n(int(v.size()));
        if(i!=0 || i==n-1) i--;
        
        std::vector<double> q(n);
        std::vector<double> s(n);
        for(int j=0;j<n-1;j++){
            double deltax(v[j+1].first-v[j].first);
            s[j]=(r[j+1]-r[j])/(3*deltax);
            q[j]=(v[j+1].second-v[j].second)/deltax-deltax*(r[i+1]+2*r[i])/3;
        }
            
        double t(fabs(x-v[i].first));
        return v[i].second+t*(q[i]+t*(r[i]+t*s[i]));
    }
};

class BarycentricInterpolator: public AbstractInterpolator{
    private:
    std::vector<double> w;
    int d;
    int max(int a, int b){
        if(a>b) return a;
        return b;
    }
    int min(int a, int b){
        if(a<b) return a;
        return b;
    }
    public:
    BarycentricInterpolator(const std::vector<std::pair<double, double>> &data, int order):AbstractInterpolator(data),d(order),w(int(data.size()),0){
        if(d<0 || d>int(v.size())) throw std::domain_error("Invalid order");
        int n(int(v.size()));
        double p(1);
        for(int i=1;i<=n;i++){
            for(int k=max(1,i-d);k<=min(i,n-d);k++){
                p=1;
                for(int j=k;j<=k+d;j++){
                    if(j!=i)
                        p=p/(v[i-1].first-v[j-1].first);
                }
                if(k%2==0) 
                    p=-p;
            }
        w[i-1]=w[i-1]+p;
        }
    }
    double operator()(double x) const override{
        int n(int(v.size()));
        double p(0),q(0),u;
        
        for(int i=1;i<=n;i++){
            if(fabs(x-v[i-1].first)<pow(10,-10)) return v[i-1].second;
            u=w[i-1]/(x-v[i-1].first);
            p=p+u*v[i-1].second;
            q=q+u;
        }
         
        return p/q;
    }
    std::vector<double> GetWeights() const{
        return w;
    }
};

template <typename FunTip>
double Limit(FunTip f, double x0, double h = 0, double eps = 1e-8, double nmax = 20){
    if(nmax<3 || nmax>30) throw std::domain_error("Invalid parameters");
    if(eps<=0) throw std::domain_error("Invalid parameters");
    if(h==0){
        if(fabs(x0)>1) h=0.001*fabs(x0);
        else h=0.001;
    }
    double Yold=std::numeric_limits<double>::infinity();
    std::vector<double> y(nmax);
    int broj_iteracija(0);
    
    if(x0==std::numeric_limits<double>::infinity() || x0==-std::numeric_limits<double>::infinity()) h=1;
    for(int i=0;i<nmax;i++){
        if(x0==std::numeric_limits<double>::infinity())
            y[i]=f(1/h);
        else if(x0==-std::numeric_limits<double>::infinity())
            y[i]=f(-1/h);
        else 
        y[i]=f(x0+h);
        double p=2;
        for(int k=i-1;k>=0;k--){
            y[k]=(p*y[k+1]-y[k])/(p-1);
            p=2*p; 
        }
        if(fabs(Yold-y[0])<eps) return y[0];
        Yold=y[i];
        h=h/2;
        broj_iteracija++;
        //throw std::logic_error("Accuracy goal is not achieved");
    }
    if(broj_iteracija>nmax) throw std::logic_error("Accuracy goal is not achieved");
    return y[0];
}



int main (){
    
    return 0;
}