//NA 2017/2018: Zadaća 2, Zadatak 1
//NA 2017/2018: Zadaća 1, Zadatak 1
#include <iostream>
#include <cmath>
#include <vector>
#include <initializer_list>
#include <stdexcept>
#include <limits>
#include <iomanip>

typedef std::vector<std::vector<double>> Matrica;

class Vector
{
private:
    std::vector<double> vek;
public:
    explicit Vector(int n) {
        if(n<=0) throw std::range_error("Bad dimension");
        for(int i=0; i<n; i++)
            vek.push_back(0);
    }

    Vector(std::initializer_list<double> l) {
        if(l.size()<=0) throw std::range_error("Bad dimension");
        for(auto it=l.begin(); it!=l.end(); it++) vek.push_back(*it);
    }

    int NElems() const {
        return vek.size();
    }

    double &operator[](int i) {
        return vek[i];
    }

    double operator[](int i) const {
        return vek[i];
    }

    double &operator()(int i) {
        if(i<1 || i>vek.size()) throw std::range_error("Invalid index");
        return vek[i-1];
    }

    double operator()(int i) const {
        if(i<1 || i>vek.size()) throw std::range_error("Invalid index");
        return vek[i-1];
    }

    double Norm() const {
        double suma(0);
        for(int i=0; i<NElems(); i++)
            suma+=pow(vek[i],2);
        return sqrt(suma);
    }

    friend double VectorNorm(const Vector &v);

    double GetEpsilon() const {
        auto ep(std::numeric_limits<double>::epsilon());
        return ep*Norm()*10;
    }

    void Print(char separator='\n', double eps=-1)const {
        for(int i=0; i<NElems(); i++) {
            if(fabs(vek[i]-GetEpsilon())<eps) std::cout<<"0";
            else std::cout<<vek[i];
            if(i!=NElems()-1) std::cout<<separator;
        }
    }

    friend void PrintVector(const Vector &v, char separator, double eps);

    friend Vector operator+(const Vector &v1, const Vector &v2);

    Vector &operator+=(const Vector &v) {
        if(NElems()!=v.NElems()) throw std::domain_error("Incompatible formats");
        for(int i=0; i<NElems(); i++)
            vek[i]+=v[i];
        return *this;
    }

    friend Vector operator-(const Vector &v1, const Vector &v2);

    Vector &operator-=(const Vector &v) {
        if(NElems()!=v.NElems()) throw std::domain_error("Incompatible formats");
        for(int i=0; i<NElems(); i++)
            vek[i]-=v[i];
        return *this;
    }

    friend Vector operator*(double s, const Vector &v);

    friend Vector operator*(const Vector &v, double s);

    Vector &operator*=(double s) {
        for(int i=0; i<NElems(); i++)
            vek[i]*=s;
        return *this;
    }

    friend double operator*(const Vector &v1, const Vector &v2);

    friend Vector operator/(const Vector &v, double s);

    Vector &operator/=(double s) {
        if(fabs(s-0)<GetEpsilon()) throw std::domain_error("Division by zero");
        for(int i=0; i<NElems(); i++)
            vek[i]/=s;
        return *this;
    }

    //nove:

    void Chop(double eps=-1) {
        if(eps<0) eps=GetEpsilon();
        for(int i=0; i<NElems(); i++) {
            if(vek[i]<eps) vek[i]=0;
        }
    }

    bool EqualTo(const Vector &v, double eps=-1) const {
        if(v.NElems()!=NElems()) return false;
        if(eps<0) eps=GetEpsilon();
        for(int i=0; i<NElems(); i++) {
            if(fabs(vek[i]-v[i])<eps) return false;
        }
        return true;
    }
};

double VectorNorm(const Vector &v){
    double suma(0);
    for(int i=0; i<v.NElems(); i++)
        suma+=pow(v[i],2);
    return sqrt(suma);
}

void PrintVector(const Vector &v, char separator='\n', double eps=-1){
    for(int i=0; i<v.NElems(); i++) {
        if(fabs(v[i]-v.GetEpsilon())<eps) std::cout<<"0";
        else std::cout<<v[i];
        if(i!=v.NElems()-1 && separator!='\n') std::cout<<separator;
        else std::cout<<separator;

    }
}

Vector operator+(const Vector &v1, const Vector &v2){
    if(v1.NElems()!=v2.NElems()) throw std::domain_error("Incompatible formats");
    Vector rezultat(v1);
    return rezultat+=v2;
}

Vector operator-(const Vector &v1, const Vector &v2){
    if(v1.NElems()!=v2.NElems()) throw std::domain_error("Incompatible formats");
    Vector rezultat(v1);
    return rezultat-=v2;
}

Vector operator*(double s, const Vector &v){
    Vector rezultat(v);
    for(int i=0; i<v.NElems(); i++)
        rezultat[i]*=s;
    return rezultat;
}

Vector operator*(const Vector &v, double s){
    Vector rezultat(v);
    for(int i=0; i<v.NElems(); i++)
        rezultat[i]*=s;
    return rezultat;
}

double operator*(const Vector &v1, const Vector &v2){
    if(v1.NElems()!=v2.NElems()) throw std::domain_error("Incompatible formats");
    double rezultat(0);
    for(int i=0; i<v1.NElems(); i++)
        rezultat+=v1[i]*v2[i];
    return rezultat;
}

Vector operator/(const Vector &v, double s){
    if(fabs(s-0)<v.GetEpsilon()) throw std::domain_error("Division by zero");
    Vector rezultat(v);
    for(int i=0; i<rezultat.NElems(); i++)
        rezultat[i]/=s;
    return rezultat;
}

class Matrix
{
private:
    Matrica M;
public:
    Matrix(int m, int n) {
        if(m<1 || n<1) throw std::range_error("Bad dimension");
        M.resize(m);
        for(int i=0; i<m; i++)
            M[i].resize(n);
    }

    Matrix(const Vector &v) {
        M.resize(1);
        for(int i=0; i<v.NElems(); i++)
            for(int j=0; j<1; j++)
                M[0].push_back(v[i]);
    }

    Matrix(std::initializer_list<std::vector<double>> l) {
        auto tmp(l.begin());
        int br_el(tmp->size());
        if(l.begin()==l.end()) throw std::range_error("Bad dimension");
        while(tmp!=l.end()) {
            if(tmp->size()==0) throw std::range_error("Bad dimension");
            if(tmp->size()!=br_el) throw std::logic_error("Bad matrix");
            tmp++;
        }
        tmp=l.begin();
        int brojac(0);
        while(tmp!=l.end()) {
            brojac++;
            tmp++;
        }
        tmp=l.begin();
        M.resize(brojac);
        int i(0);
        while (tmp!=l.end()) {
            M[i]=*tmp;
            i++;
            tmp++;
        }
    }

    int NRows() const {
        return M.size();
    }

    int NCols() const {
        return M[0].size();
    }

    double *operator[](int i) {
        return &M[i][0];
    }

    const double *operator[](int i) const {
        return &M[i][0];
    }

    double &operator()(int i, int j) {
        if(i<1 || i>NRows() || j<1 || j>NCols()) throw std::range_error("Invalid index");
        return M[i-1][j-1];
    }

    double operator ()(int i, int j) const {
        if(i<1 || i>NRows() || j<1 || j>NCols()) throw std::range_error("Invalid index");
        return M[i-1][j-1];
    }

    double Norm() const {
        double suma(0);
        for(int i=0; i<NRows(); i++)
            for(int j=0; j<NCols(); j++)
                suma+=pow(M[i][j],2);
        return sqrt(suma);
    }

    friend double MatrixNorm(const Matrix &m);

    double GetEpsilon() const {
        auto ep(std::numeric_limits<double>::epsilon());
        return ep*Norm()*10;
    }

    void Print(int width=10, double eps=-1)const {
        for(int i=0; i<NRows(); i++) {
            for(int j=0; j<NCols(); j++) {
                if(fabs(M[i][j]-GetEpsilon())<eps) std::cout<<std::setw(width)<<"0";
                else std::cout<<std::setw(width)<<M[i][j];
            }
            std::cout<<std::endl;
        }
    }

    friend void PrintMatrix(const Matrix &m, int width, double eps);

    friend Matrix operator+(const Matrix &m1, const Matrix &m2);

    Matrix &operator+=(const Matrix &m) {
        if(NRows()!=m.NRows() || NCols()!=m.NCols()) throw std::domain_error("Incompatible formats");
        for(int i=0; i<NRows(); i++)
            for(int j=0; j<NCols(); j++)
                M[i][j]+=m[i][j];
        return *this;
    }

    friend Matrix operator-(const Matrix &m1, const Matrix &m2);

    Matrix &operator-=(const Matrix &m) {
        if(NRows()!=m.NRows() || NCols()!=m.NCols()) throw std::domain_error("Incompatible formats");
        for(int i=0; i<NRows(); i++)
            for(int j=0; j<NCols(); j++)
                M[i][j]-=m[i][j];
        return *this;
    }

    friend Matrix operator*(double s, const Matrix &m);

    friend Matrix operator*(const Matrix &m, double s);

    Matrix &operator*=(double s) {
        for(int i=0; i<NRows(); i++)
            for(int j=0; j<NCols(); j++)
                M[i][j]*=s;
        return *this;
    }

    friend Matrix operator*(const Matrix &m1, const Matrix &m2);

    Matrix &operator*=(const Matrix &m) {
        if(NCols()!=m.NRows()) throw std::domain_error("Incompatible formats");
        Matrix rezultat(NRows(),m.NCols());
        for(int i=0; i<NRows(); i++)
            for(int j=0; j<m.NCols(); j++)
                for(int k=0; k<NCols(); k++)
                    rezultat[i][j]+=M[i][k]*m[k][j];
        return *this=rezultat;
    }

    friend Vector operator*(const Matrix &m, const Vector &v);

    friend Matrix Transpose(const Matrix &m);

    void Transpose() {
        if(NRows()==NCols()) {
            for(int i=0; i<NCols(); i++) {
                for(int j=i; j<NRows(); j++) {
                    auto tmp(M[i][j]);
                    M[i][j]=M[j][i];
                    M[j][i]=tmp;
                }
            }
        } else {
            Matrix tmp(NCols(),NRows());
            for(int i=0; i<NCols(); i++)
                for(int j=0; j<NRows(); j++)
                    tmp[i][j]=M[j][i];
            *this=tmp;
        }
    }

    //nove:

    void Chop(double eps=-1) {
        if(eps<0) eps=GetEpsilon();
        for(int i=0; i<NRows(); i++) {
            for(int j=0; j<NCols(); j++) {
                if(M[i][j]<eps) M[i][j]=0;
            }
        }
    }

    bool EqualTo(const Matrix &m, double eps=-1)const {
        if(m.NRows()!=NRows() || m.NCols()!=NCols()) return false;
        if(eps<0) eps=GetEpsilon();
        for(int i=0; i<NRows(); i++) {
            for(int j=0; j<NCols(); j++) {
                if(fabs(M[i][j]-m[i][j])<eps) return false;
            }
        }
        return true;
    }

    friend Matrix LeftDiv(Matrix m1, Matrix m2) {
        if(m1.NRows()!=m1.NCols()) throw std::domain_error("Divisor matrix is not square");
        if(m2.NRows()!=m1.NRows()) throw std::domain_error("Incompatible formats");
        for(int k=0; k<m1.NRows(); k++) {
            int p(k);
            for(int i=k+1; i<m2.NCols(); i++) {
                if(abs(m1[i][k])>abs(m1[p][k])) {
                    p=i;
                }
            }
            if(abs(m1[p][k])<m1.GetEpsilon()) throw std::domain_error("Divisor matrix is singular");
            if(p!=k) {
                //razmijeni k-ti i p-ti red u matrici m1
                for(int i=0; i<m1.NRows(); i++) {
                    for(int j=0; j<m1.NCols(); j++) {
                        if(i==k) {
                            std::swap(m1[k][j],m1[p][j]);
                        }
                    }
                }
                //razmijeni k-ti i p-ti red u matrici m2
                for(int i=0; i<m2.NRows(); i++) {
                    for(int j=0; j<m2.NCols(); j++) {
                        if(i==k) {
                            std::swap(m2[k][j],m2[p][j]);
                        }
                    }
                }
            }
            for(int i=k+1; i<m1.NRows(); i++) {
                double mi(m1[i][k]/m1[k][k]);
                for(int j=k+1; j<m1.NRows(); j++)
                    m1[i][j]=m1[i][j]-mi*m1[k][j];
                for(int j=0; j<m2.NCols(); j++)
                    m2[i][j]=m2[i][j]-mi*m2[k][j];
            }
        }
        return m1;
    }

    friend Vector LeftDiv(Matrix m, Vector v){
        if(m.NRows()!=m.NCols()) throw std::domain_error("Divisor matrix is not square");
        if(m.NRows()!=v.NElems()) throw std::domain_error("Incompatible formats");
        Vector x(v.NElems());
        for(int i=0;i<v.NElems();i++)
        x[i]=v[i];
        
        Matrix a(m.NRows(),m.NCols());
        for(int i=0;i<m.NRows();i++){
            for(int j=0;j<m.NCols();j++){
                a[i][j]=m[i][j];
            }
        }
        
        for(int k=0; k<m.NRows(); k++) {
            int p(k);
            for(int i=k+1; i<m.NRows(); i++) {
                if(fabs(a[i][k])>fabs(a[p][k])) {
                    p=i;
                }
            }
            if(fabs(a[p][k])<m.GetEpsilon()) throw std::domain_error("Divisor matrix is singular");
            if(p!=k) {
                //razmijeni k-ti i p-ti red u matrici m
                for(int i=0; i<m.NRows(); i++) {
                    for(int j=0; j<m.NCols(); j++) {
                        if(i==k) {
                            std::swap(a[k][j],a[p][j]);
                        }
                    }
                }
                //razmijeni k-ti i p-ti element u vektoru v
                for(int i=0; i<v.NElems(); i++) {
                        if(i==k) {
                            std::swap(x[k],x[p]);
                        }
                }
            }
            for(int i=k+1; i<m.NRows(); i++) {
                double mi(a[i][k]/a[k][k]);
                for(int j=k+1; j<m.NRows(); j++)
                    a[i][j]=a[i][j]-mi*a[k][j];
                for(int j=0; j<m.NCols(); j++)
                    x[j]=x[j]-mi*x[k];
            }
        }
        return x;
    }

    friend Matrix operator/(const Matrix &m, double s) {
        if(fabs(s)<m.GetEpsilon()) throw std::domain_error("Division by zero");
        Matrix rezultat(m.NRows(),m.NCols());
        for(int i=0; i<m.NRows(); i++) {
            for(int j=0; j<m.NCols(); j++) {
                rezultat[i][j]=m[i][j]/s;
            }
        }
        return rezultat;
    }

    Matrix &operator/=(double s) {
        if(fabs(s)<GetEpsilon()) throw std::domain_error("Division by zero");
        Matrix rezultat(NRows(),NCols());
        for(int i=0; i<NRows(); i++) {
            for(int j=0; j<NCols(); j++) {
                M[i][j]/=s;
            }
        }
        return *this;;
    }

    friend Matrix operator /(Matrix m1, Matrix m2) {
        if(m2.NCols()!=m2.NRows()) throw std::domain_error("Divisor matrix is not square");
        if(m2.NCols()!=m1.NCols()) throw std::domain_error("Incompatible formats");
        for(int k=0; k<m2.NRows(); k++) {
            int p(k);
            for(int i=k+1; i<m2.NCols(); i++) {
                if(fabs(m2[i][k])>fabs(m2[p][k])) {
                    p=i;
                }
            }
            if(fabs(m2[p][k])<m2.GetEpsilon()) throw std::domain_error("Divisor matrix is singular");
            if(p!=k) {
                //razmijeni k-ti i p-ti red u matrici m1
                for(int i=0; i<m1.NRows(); i++) {
                    for(int j=0; j<m1.NCols(); j++) {
                        if(i==k) {
                            std::swap(m1[k][j],m1[p][j]);
                        }
                    }
                }
                //razmijeni k-ti i p-ti red u matrici m2
                for(int i=0; i<m2.NRows(); i++) {
                    for(int j=0; j<m2.NCols(); j++) {
                        if(i==k) {
                            std::swap(m2[k][j],m2[p][j]);
                        }
                    }
                }
            }
            for(int i=k+1; i<m2.NRows(); i++) {
                double mi(m1[i][k]/m1[k][k]);
                for(int j=k+1; j<m2.NRows(); j++)
                    m2[j][i]=m2[j][i]-mi*m2[j][k];
                for(int j=0; j<m1.NCols(); j++)
                    m1[j][i]=m1[j][i]-mi*m1[j][k];
            }
        }
        return m1;
    }

    Matrix &operator/=(Matrix m){
        if(m.NRows()!=m.NCols()) throw std::domain_error("Divisor matrix is not square");
        if(NCols()!=m.NRows()) throw std::domain_error("Incompatible formats");
        for(int k=0; k<m.NRows(); k++) {
            int p(k);
            for(int i=k+1; i<m.NRows(); i++) {
                if(fabs(m[i][k])>fabs(m[p][k])) {
                    p=i;
                }
            }
            if(fabs(m[p][k])<m.GetEpsilon()) throw std::domain_error("Divisor matrix is singular");
            if(p!=k) {
                //razmijeni k-ti i p-ti red u matrici m1
                for(int i=0; i<NRows(); i++) {
                    for(int j=0; j<NCols(); j++) {
                        if(i==k) {
                            std::swap(M[k][j],M[p][j]);
                        }
                    }
                }
                //razmijeni k-ti i p-ti red u matrici m2
                for(int i=0; i<m.NRows(); i++) {
                    for(int j=0; j<m.NCols(); j++) {
                        if(i==k) {
                            std::swap(m[k][j],m[p][j]);
                        }
                    }
                }
            }
            for(int i=k+1; i<m.NRows(); i++) {
                double mi(m[i][k]/m[k][k]);
                for(int j=k+1; j<m.NRows(); j++)
                    m[j][i]=m[j][i]-mi*m[j][k];
                for(int j=0; j<NCols(); j++)
                    M[j][i]=M[j][i]-mi*M[j][k];
            }
        }
        return *this;
    }
    
    double Det() const{
        if(NCols()!=NRows()) throw std::domain_error("Matrix is not square");
        int d=1;
        Matrix a(NRows(),NCols());
        for(int i=0;i<NRows();i++){
            for(int j=0;j<NCols();j++){
                a[i][j]=M[i][j];
            }
        }
        
        for(int k=0;k<NRows();k++){
            int p=k;
            for(int i=k+1;i<NRows();i++){
                if(a[i][k]>a[p][k]){
                    p=i;
                }
            }
            if(fabs(a[p][k])<a.GetEpsilon()) return 0;
            if(p!=k){
                for(int i=0;i<NRows();i++)
                    for(int j=0;j<NCols();j++)
                      if(i==k)  std::swap(a[k][j],a[p][j]);
                d=-d;
            }
            d*=a[k][k];
            for(int i=k+1;i<NRows();i++){
                double mi=a[i][k]/a[k][k];
                for(int j=k+1;j<NRows();j++)
                    a[i][j]=a[i][j]-mi*a[k][j];
            }
        }
        return d;
    } //nap
    
    friend double Det(Matrix m);
    
    void Invert(){
        if(NCols()!=NRows()) throw std::domain_error("Matrix is not square");
        int w,p;
        for(int k=0;k<NRows();k++){
             p=k;
            for(int i=k+1;i<NRows();i++){
                if(M[i][k]>M[p][k]) p=i;
            }
            if(fabs(M[p][k])<GetEpsilon()) throw std::domain_error("Matrix is singular");
            if(p!=k){
                for(int i=0;i<NRows();i++)
                    for(int j=0;j<NCols();j++)
                      if(i==k)  std::swap(M[k][j],M[p][j]);
            }
            w=p;
            double mi=M[k][k];
            M[k][k]=1;
            for(int j=0;j<NRows();j++)
                M[k][j]=M[k][j]/mi;
            for(int i=0;i<NRows();i++){
                if(i!=k){
                    mi=M[i][k];
                    M[i][k]=0;
                    for(int j=0;j<NRows();j++)
                        M[i][j]=M[i][j]-mi*M[k][j];
                }
            }
        }
        for(int j=NRows();j>0;j--){
            p=w;
            if(p!=j){
                for(int k=0;k<NRows();k++)
                for(int i=0;i<NCols();i++){
                    std::swap(M[i][p],M[i][j]);
                }
            }
        }
    }
    
    friend Matrix Inverse(Matrix m); //nap
    
    void ReduceToRREF();
    friend Matrix RREF(Matrix m);
    
    int Rank() const{
        if(NCols()!=NRows()) throw std::domain_error("Matrix is not square");
        
         Matrix a(NRows(),NCols());
        for(int i=0;i<NRows();i++){
            for(int j=0;j<NCols();j++){
                a[i][j]=M[i][j];
            }
        }
        
        int k(0),l(0),p; double v;
        //bool w;
        //for(int j=0;j<a.NRows();j++)
          //  w=false;
        while(k<a.NRows() && l<a.NCols()){
            l=l+1; 
            k=k+1;
            v=0; 
            while(v<GetEpsilon() && l<a.NCols()){
                p=k;
                for(int i=k;i<a.NCols();i++)
                        if(a[i][l]>v){
                            v=a[i][l];
                            p=i;
                        }
                if(v<a.GetEpsilon()) l=l+1;
            }
        //} 
        if(l<a.NCols()) { //w=true;
            if(p!=k){
                for(int i=0;i<a.NRows();i++){
                    for(int j=0;j<a.NCols();j++){
                        if(i==k) std::swap(a[k][j],a[p][j]);
                    }
                }
            } 
        double mi=a[k][l];
        for(int j=l;j<a.NRows();j++)
            a[k][j]=a[k][j]/mi;
        for(int i=0;i<a.NCols();i++){
            if(i!=k){
                mi=a[i][l];
                    for(int j=l;j>a.NRows();j++)
                        a[i][j]=a[i][j]-mi*a[k][j];
                    }
                }
            }
    }
    return k;
}
    
    friend int Rank(Matrix m);
};

double MatrixNorm(const Matrix &m){
    double suma(0);
    for(int i=0; i<m.NRows(); i++)
        for(int j=0; j<m.NCols(); j++)
            suma+=pow(m[i][j],2);
    return sqrt(suma);
}

void PrintMatrix(const Matrix &m, int width=10, double eps=-1){
    for(int i=0; i<m.NRows(); i++) {
        for(int j=0; j<m.NCols(); j++) {
            if(fabs(m[i][j]-m.GetEpsilon())<eps) std::cout<<std::setw(width)<<"0";
            else std::cout<<std::setw(width)<<m[i][j];
        }
        std::cout<<std::endl;
    }
}

Matrix operator+(const Matrix &m1, const Matrix &m2){
    if(m1.NRows()!=m2.NRows() || m1.NCols()!=m2.NCols()) throw std::domain_error("Incompatible formats");
    Matrix rezultat(m1.NRows(),m1.NCols());
    for(int i=0; i<rezultat.NRows(); i++)
        for(int j=0; j<rezultat.NCols(); j++)
            rezultat[i][j]=m1[i][j]+m2[i][j];
    return rezultat;
}

Matrix operator-(const Matrix &m1, const Matrix &m2){
    if(m1.NRows()!=m2.NRows() || m1.NCols()!=m2.NCols()) throw std::domain_error("Incompatible formats");
    Matrix rezultat(m1.NRows(),m1.NCols());
    for(int i=0; i<rezultat.NRows(); i++)
        for(int j=0; j<rezultat.NCols(); j++)
            rezultat[i][j]=m1[i][j]-m2[i][j];
    return rezultat;
}

Matrix operator*(double s, const Matrix &m){
    Matrix rezultat(m.NRows(),m.NCols());
    for(int i=0; i<m.NRows(); i++)
        for(int j=0; j<m.NCols(); j++)
            rezultat[i][j]=m[i][j]*s;
    return rezultat;
}

Matrix operator*(const Matrix &m, double s){
    Matrix rezultat(m.NRows(),m.NCols());
    for(int i=0; i<m.NRows(); i++)
        for(int j=0; j<m.NCols(); j++)
            rezultat[i][j]=m[i][j]*s;
    return rezultat;
}

Matrix operator*(const Matrix &m1, const Matrix &m2){
    if(m1.NCols()!=m2.NRows()) throw std::domain_error("Incompatible formats");
    Matrix rezultat(m1.NRows(),m2.NCols());
    for(int i=0; i<m1.NRows(); i++)
        for(int j=0; j<m2.NCols(); j++)
            for(int k=0; k<m1.NCols(); k++)
                rezultat[i][j]+=m1[i][k]*m2[k][j];
    return rezultat;
}

Vector operator*(const Matrix &m, const Vector &v){
    if(m.NCols()!=v.NElems()) throw std::domain_error("Incompatible formats");
    Vector rezultat(m.NRows());
    for(int i=0; i<m.NRows(); i++)
        for(int j=0; j<m.NCols(); j++)
            rezultat[i]+=m[i][j]*v[j];
    return rezultat;
}

Matrix Transpose(const Matrix &m){
    Matrix tmp(m.NCols(),m.NRows());
    for(int i=0; i<m.NCols(); i++)
        for(int j=0; j<m.NRows(); j++)
            tmp[i][j]=m[j][i];
    return tmp;
}

double Det(Matrix m){
        if(m.NCols()!=m.NRows()) throw std::domain_error("Matrix is not square");
        int d=1;
        Matrix a(m.NRows(),m.NCols());
        for(int i=0;i<m.NRows();i++){
            for(int j=0;j<m.NCols();j++){
                a[i][j]=m[i][j];
            }
        }
        
        for(int k=0;k<m.NRows();k++){
            int p=k;
            for(int i=k+1;i<m.NRows();i++){
                if(a[i][k]>a[p][k]){
                    p=i;
                }
            }
            if(fabs(a[p][k])<a.GetEpsilon()) return 0;
            if(p!=k){
                for(int i=0;i<m.NRows();i++)
                    for(int j=0;j<m.NCols();j++)
                      if(i==k)  std::swap(a[k][j],a[p][j]);
                d=-d;
            }
            d*=a[k][k];
            for(int i=k+1;i<m.NRows();i++){
                double mi=a[i][k]/a[k][k];
                for(int j=k+1;j<m.NRows();j++)
                    a[i][j]=a[i][j]-mi*a[k][j];
            }
        }
        return d;
} //nap

Matrix Inverse(Matrix m){
        if(m.NCols()!=m.NRows()) throw std::domain_error("Matrix is not square");
        int w,p;
        for(int k=0;k<m.NRows();k++){
             p=k;
            for(int i=k+1;i<m.NRows();i++){
                if(m[i][k]>m[p][k]) p=i;
            }
            if(fabs(m[p][k])<m.GetEpsilon()) throw std::domain_error("Matrix is singular");
            if(p!=k){
                for(int i=0;i<m.NRows();i++)
                    for(int j=0;j<m.NCols();j++)
                      if(i==k)  std::swap(m[k][j],m[p][j]);
            }
            w=p;
            double mi=m[k][k];
            m[k][k]=1;
            for(int j=0;j<m.NRows();j++)
                m[k][j]=m[k][j]/mi;
            for(int i=0;i<m.NRows();i++){
                if(i!=k){
                    mi=m[i][k];
                    m[i][k]=0;
                    for(int j=0;j<m.NRows();j++)
                        m[i][j]=m[i][j]-mi*m[k][j];
                }
            }
        }
        for(int j=m.NRows()-1;j>=0;j--){
            p=w;
            if(p!=j){
                for(int i=0;i<m.NRows();i++){
                    std::swap(m[i][p],m[i][j]);
                }
            }
        }
        return m;
}
    


int Rank(Matrix m){
        if(m.NCols()!=m.NRows()) throw std::domain_error("Matrix is not square");
        
         Matrix a(m.NRows(),m.NCols());
        for(int i=0;i<m.NRows();i++){
            for(int j=0;j<m.NCols();j++){
                a[i][j]=m[i][j];
            }
        }
        
        int k(0),l(0),p; double v;
        //bool w;
        //for(int j=0;j<a.NRows();j++)
          //  w=false;
        while(k<a.NRows() && l<a.NCols()){
            l=l+1; 
            k=k+1;
            v=0; 
            while(v<m.GetEpsilon() && l<a.NCols()){
                p=k;
                for(int i=k;i<a.NCols();i++)
                        if(a[i][l]>v){
                            v=a[i][l];
                            p=i;
                        }
                if(v<a.GetEpsilon()) l=l+1;
            }
        //} 
        if(l<a.NCols()) { //w=true;
            if(p!=k){
                for(int i=0;i<a.NRows();i++){
                    for(int j=0;j<a.NCols();j++){
                        if(i==k) std::swap(a[k][j],a[p][j]);
                    }
                }
            } 
        double mi=a[k][l];
        for(int j=l;j<a.NRows();j++)
            a[k][j]=a[k][j]/mi;
        for(int i=0;i<a.NCols();i++){
            if(i!=k){
                mi=a[i][l];
                    for(int j=l;j>a.NRows();j++)
                        a[i][j]=a[i][j]-mi*a[k][j];
                    }
                }
            }
    }
    return k;
}

class LUDecomposer{
    private:
    Matrix a;
    Vector w;
    public:
        LUDecomposer(Matrix m):a(m), w(m.NRows()) {
            if(m.NCols()!=m.NRows()) throw std::domain_error("Matrix is not square");
            int imax; double big, tmp,d=1.0;
            Vector pom(m.NRows());
            for(int i=0;i<m.NRows();i++){
                big=0.;
                for(int j=0;j<m.NRows();j++)
                    if((tmp=a[i][j])>big) big=tmp;
                if((big-0.)<a.GetEpsilon()) throw std::domain_error("Matrix is singular");
                pom[i]=1./big;
            }
            for(int k=0;k<m.NRows();k++){
                big=0;
                for(int i=k;i<m.NRows();i++){
                    tmp=pom[i]*fabs(a[i][k]);
                    if(tmp>big){
                        big=tmp;
                        imax=i;
                    }
                }
                if(k!=imax){
                    for(int j=0;j<m.NCols();j++)
                        std::swap(a[imax][j],a[k][j]);
                    d=-d;
                    pom[imax]=pom[k];
                }
                w[k]=imax;
                if(a[k][k]<a.GetEpsilon()) a[k][k]=a.GetEpsilon();
                for(int i=k+1;i<m.NRows();i++){
                    tmp=a[i][k]/=a[k][k];
                    for(int j=k+1;j<m.NCols();j++)
                        a[i][j]-=tmp*a[k][k];
                }
            }
        }
        
        void Solve(const Vector &b, Vector &x) const{
            if(b.NElems()!=x.NElems()) throw std::domain_error("Incompatible formats");
            int ii=0,ip;
            double suma;
            for(int i=0;i<b.NElems();i++) x[i]=b[i];
            for(int i=0;i<b.NElems();i++){
                ip=w[i];
                suma=x[ip];
                x[ip]=x[i];
                if(ii!=0)
                    for(int j=ii-1;j<i;j++) suma-=a[i][j]*x[j];
                else if((suma-0.)<a.GetEpsilon()) ii=i+1;
                x[i]=suma;
            }
            for(int i=b.NElems()-1;i>=0;i--){
                suma=x[i];
                for(int j=i+1;j<b.NElems();j++)
                    suma-=a[i][j]*x[j];
                x[i]=suma/a[i][i];
            }
            
        }
        
        Vector Solve(Vector b) const{
            if(b.NElems()!=w.NElems()) throw std::domain_error("Incompatible formats");
            int ii=0,ip;
            double suma;
            Vector x(b.NElems());
            for(int i=0;i<b.NElems();i++) x[i]=b[i];
            for(int i=0;i<b.NElems();i++){
                ip=w[i];
                suma=x[ip];
                x[ip]=x[i];
                if(ii!=0)
                    for(int j=ii-1;j<i;j++) suma-=a[i][j]*x[j];
                else if((suma-0.)<a.GetEpsilon()) ii=i+1;
                x[i]=suma;
            }
            for(int i=b.NElems()-1;i>=0;i--){
                suma=x[i];
                for(int j=i+1;j<b.NElems();j++)
                    suma-=a[i][j]*x[j];
                x[i]=suma/a[i][i];
            }
            return x;
        }
        
        void Solve(Matrix &b, Matrix &x) const{
            if(b.NRows()!=x.NRows() || b.NCols()!=x.NCols()) throw std::domain_error("Incompatible formats");
            Vector pom(b.NRows());
            for(int j=0;j<b.NCols();j++){
                for(int i=0;i<b.NRows();i++)
                    pom[i]=b[i][j];
                    Solve(pom,pom);
                    for(int i=0;i<b.NRows();i++) x[i][j]=pom[i];
            }
        }
        
        Matrix Solve(Matrix b) const{
            if(b.NRows()!=a.NRows() || b.NCols()!=a.NCols()) throw std::domain_error("Incompatible formats");
            Vector pom(b.NRows()); Matrix x(b.NRows(),b.NCols());
            for(int j=0;j<b.NCols();j++){
                for(int i=0;i<b.NRows();i++)
                    pom[i]=b[i][j];
                    Solve(pom,pom);
                    for(int i=0;i<b.NRows();i++) x[i][j]=pom[i];
            }
            return x;
        }
        
        Matrix GetCompactLU() const{
            return a;
        }
        
        Matrix GetL() const;
        Matrix GetU() const;
        
        Vector GetPermuation() const{
            return w;
        }
};

/*class QRDecomposer{
    private:
    Matrix qt,r;
    public:
    QRDecomposer(Matrix m):qt(m.NRows(),m.NCols()),r(m){
        if(m.NCols()!=m.NRows()) throw std::domain_error("Matrix is not square");
        bool sing(false); 
        Vector c(m.NRows()),d(m.NRows());
        double scale, sigma, sum, tau;
        for(int k=0;k<m.NRows();k++){
            scale=0;
            for(int i=k;i<m.NRows();i++)
                scale=std::max(scale, fabs(r[i][k]));
            if(scale==0.0){
                sing=true;
                c[k]=d[k]=0;
            }
            else{
                for(int i=k;i<m.NRows();i++) r[i][k]/=scale;
                sum=0;
                for(i=k;i<m.NRows();i++) sum+=sqrt(r[i][k]);
                sigma=std::sign(sqrt(sum),r[])
            }
        }
    }
    void Solve(const Vector &b, Vector &x) const;
    Vector Solve(Vector b) const;
    void Solve(Matrix &b, Matrix &x) const;
    Matrix Solve(Matrix b) const;
    Vector MulQWith(Vector v) const;
    Matrix MulQWith(Matrix m) const;
    Vector MulQTWith(Vector v) const;
    Matrix MulQTWith(Matrix m) const;
    Matrix GetQ() const;
    Matrix GetR() const;
};*/

int main (){
    
     try
    {
        Matrix M1 {{1,0,0},{0,0,1}}, M2 {{1,0},{0,1}};
        Matrix M=LeftDiv(M1,M2);
    }
    catch (std::domain_error izuzetak)
    {
        std::cout<<izuzetak.what()<<std::endl;
    }
    try
    {
        Matrix M1 {{1,2},{3,4},{5,6},{7,8}}, M2 {{1,2},{3,4},{5,6},{7,8},{9,10}};
        Matrix M=LeftDiv(M1,M2);
    }
    catch (std::domain_error izuzetak)
    {
        std::cout<<izuzetak.what()<<std::endl;
    }
    try
    {
        Matrix M1 {{10,10,10},{10,10,0},{10,10,10}}, M2 {{10,10},{10,10},{10,10}};
        Matrix M=LeftDiv(M1,M2);
    }
    catch (std::domain_error izuzetak)
    {
        std::cout<<izuzetak.what()<<std::endl;
    }
    
    
    
    return 0;
}