//NA 2017/2018: Zadaća 4, Zadatak 1
#include <iostream>
#include <cmath>
#include <vector>
#include <stdexcept>
#include <utility>
#include <limits>

class ChebyshevApproximation{
    private:
    int n,m;
    std::vector<double> c;
    double xmin, xmax;
    const double pi=4*atan(1);
    
    ChebyshevApproximation(std::vector<double> &c, double xmin, double xmax, int nn, int mm): n(nn), m(mm), c(c), xmin(xmin), xmax(xmax){}
    public:
    template <typename FunTip>
    ChebyshevApproximation(FunTip f, double xmin, double xmax, int n):n(n),m(n), c(n), xmin(xmin), xmax(xmax) {
        if(xmin>=xmax || n<=1) throw std::domain_error("Bad parameters");
        std::vector<double> vek(n);
        double bma(0.5*(xmax-xmin)), bpa(0.5*(xmin+xmax)),y,fac,sum;
        for(int i=0; i<n; i++){
            y=std::cos(pi*(i+0.5)/n);
            vek[i]=f(y*bma+bpa);
        }
        fac=2.0/n;
        for(int i=0; i<n; i++){
            sum=0.;
            for(int j=0; j<n; j++)
                sum+=vek[j]*std::cos(pi*i*(j+0.5)/n);
            c[i]=fac*sum;
        }
     } 
    void set_m(int m){
        if(m<=1 || m>n) throw std::domain_error("Bad order");
        //while(m>1 && abs(c[m-1])<m) m--;
        this->m=m;
    } 
    void trunc(double eps){
        if(eps<=0) throw std::domain_error("Bad tolerance");
            for(int k=0; k<n; k++){
                if(fabs(c[k])<eps){
                    m=k;
                    if(m<=1) throw std::domain_error("Bad tolerance");
                    break;
                }
            }
    } 
    double operator()(double x) const{
        if(!(x>xmin && x<xmax)) throw std::domain_error("Bad argument");
        
        double d(0.), dd(0.), sv, y, y2;
        y=(2.0*x-xmin-xmax)/(xmax-xmin);
        y2=2.0*y;
        for(int j=m-1; j>0; j--){
            sv=d;
            d=y2*d-dd+c[j];
            dd=sv;
        }
        return y*d-dd+0.5*c[0];
    } 
    double derivative(double x) const{
        if(!(x>xmin && x<xmax)) throw std::domain_error("Bad argument");
        double t, p, q, s, r;
        t=(2*x-xmax-xmin)/(xmax-xmin);
        p=1;
        q=4*t;
        s=c[1]+4*c[2]*t;
        for(int k=3; k<=m; k++){
            r=k*(2*t*q/(k-1)-p/(k-2));
            s+=c[k]*r;
            p=q;
            q=r;
        }
        return 2*s/(xmax-xmin);
    } 
    ChebyshevApproximation derivative() const{
        volatile double con(4/(xmax-xmin));
        std::vector<double> cder(int(c.size()));
        cder[m-1]=con*m*c[m];
        cder[m-2]=con*(m-1)*c[m-1];
        for(int j=m-3; j>=0; j--){
            cder[j]=cder[j+2]+con*(j+1)*c[j+1];
        }
        return ChebyshevApproximation(cder, xmin, xmax, n, m);
    } 
    ChebyshevApproximation antiderivative() const{
        std::vector<double> ck;
        for(int k=1; k<=m+1; k++){
            double pom;
            if(k+1>m) pom=0;
            else pom=c[k+1];
            ck.push_back(((xmax-xmin)/(4*k))*(pom-c[k-1]));
        }
        return ChebyshevApproximation(ck,xmin,xmax,n,m);
    }
    double integrate(double a, double b) const{
        if(!(a>=xmin && b<=xmax)) throw std::domain_error("Bad interval");
        double sum(0.), fac=1., con;
        std::vector<double> cint(n);
        con=0.25*(b-a);
        for(int j=1; j<=m; j++){
            cint[j]=con*(c[j-1]-c[j+1])/j;
            sum+=fac*cint[j];
            fac=-fac;
        }
        cint[n-1]=con*c[n-2]/(n-1);
        sum+=fac*cint[n-1];
        cint[0]=2.0*sum;
        return cint[0];
    } 
    double integrate() const{
        double sum(0.);
        for(int i=1; i<=(m+1)/2; i++)
            sum+=2*c[2*i]/(1-4*i*i);
        return (c[0]*(xmax-xmin)/2+sum*(xmax-xmin)/2);
        
    } 
};

template <typename FunTip>
std::pair<double, bool> RombergIntegration(FunTip f, double a, double b, double eps = 1e-8, int nmax = 1000000, int nmin = 50){
    if(eps<0 || nmax<0 || nmin<0 || nmax<=nmin) throw std::domain_error("Bad parameter");
    std::pair<double, bool> rezultat;
    int N(2);
    double p(0);
    double h((b-a)/N), s((f(a)+f(b))/2);
    std::vector<double> I(1,0);
    double Iold(s);
    for(int i=1; N<=nmax; i++){
        for(int j=1; j<=N/2; j++){
            s+=f(a+(2*j-1)*h);
        }
        I.push_back(h*s);
        p=4;
        for(int k=i-1; k>=1; k--){
            I[k]=(p*I[k+1]-I[k])/(p-1);
            p*=4;
        }
        if(fabs(I[1]-Iold)<=eps && N>=nmin) { 
            rezultat.first=I[1];
            rezultat.second=true;
            return rezultat;
        }
        Iold=I[1];
        h/=2;
        N*=2;
    }
    rezultat.first=I[1];
    rezultat.second=false;
    return rezultat;
}

template <typename FunTip>
std::pair<double, bool> AdaptiveAux(FunTip f, double a, double b, double eps,double f1, double f2, double f3, double R){
     std::pair<double, bool> rezultat;
     double c((a+b)/2);
     auto I1((b-a)*(f1+4*f3+f2)/6);
     auto f4(f((a+c)/2));
     auto f5(f((c+b)/2));
     if(!std::isfinite(f4)) f4=0;
     if(!std::isfinite(f5)) f5=0;
     auto I2((b-a)*(f1+4*f4+2*f3+4*f5+f2)/12);
     if(fabs(I1-I2)<=eps) {
         rezultat.first=I2;
         rezultat.second=true;
         return rezultat;
     }
     else if(R<=0){
         rezultat.first=I2;
         rezultat.second=false;
         return rezultat;
     }
     auto prvi(AdaptiveAux(f,a,c,eps,f1,f3,f4,R-1));
     auto drugi(AdaptiveAux(f,c,b,eps,f3,f2,f5,R-1));
     rezultat.first=prvi.first+drugi.first;
     rezultat.second=prvi.second && drugi.second;
     return rezultat;
 }

template <typename FunTip>
 std::pair<double, bool> AdaptiveIntegration(FunTip f, double a, double b, double eps = 1e-10, int maxdepth = 30, int nmin = 1){
    if(eps<0 || maxdepth<0 ||nmin<1) throw std::domain_error("Bad parameter");
    std::pair<double, bool> pomrezultat,rezultat;
    double h, s(0);
    bool tacnost(1);
    h=(b-a)/nmin;
    for(int i=1; i<=nmin; i++){
        double pom1(f(a)), pom2(f(a+h)), pom3(f(a+h/2));
        if(!std::isfinite(pom1)) pom1=0;
        if(!std::isfinite(pom2)) pom2=0;
        if(!std::isfinite(pom3)) pom3=0;
        pomrezultat=AdaptiveAux(f,a,a+h,eps,pom1,pom2,pom3,maxdepth);
        s+=pomrezultat.first;
        a+=h;
        tacnost=tacnost && pomrezultat.second;
    }
    rezultat.first=s;
    rezultat.second=tacnost;
    return rezultat;
}

template <typename FunTip>
 std::pair<double, bool> TanhSinhIntegration(FunTip f, double a, double b, double eps = 1e-8, int nmax = 1000000, int nmin = 20, double range = 3.5){
    if(nmin>=nmax || nmin<0|| nmax<0 || eps<0 || range<0) throw std::domain_error("Bad parameter");
    const double PI=4*std::atan(1);
    std::pair<double, bool> rezultat;
    auto fja=[f,a,b,PI](double y){
        return(((b-a)*PI)/4 * std::cosh(y)/((std::cosh((PI/2)*std::sinh(y)))*(std::cosh((PI/2)*std::sinh(y)))))*f((a+b)/2+((b-a)/2)*std::tanh((PI/2)*std::sinh(y)));
    };
    int N(2);
    double h((2*range)/N), pom1(fja(-range)), pom2(fja(range));
    if(!std::isfinite(pom1)) pom1=0;
    if(!std::isfinite(pom2)) pom2=0;
    double s((pom1+pom2)/2), Iold(s), I(0);
    while(N<nmax){
        for(int i=1; i<=N/2; i++){
            double tmp(fja(-range+(2*i-1)*h));
            if(!std::isfinite(tmp)) tmp=0;
            s+=tmp;
        }
        I=h*s;
        if(fabs(I-Iold)<=eps && N>=nmin){
            rezultat.first=I;
            rezultat.second=true;
            return rezultat;
        }
        Iold=I;
        N*=2;
        h/=2;
    }
    rezultat.first=I;
    rezultat.second=false;
    return rezultat;
 }

int main (){
    
    std::cout<<"RombergIntegration"<<std::endl;
    try{
        const double PI = 4 * std::atan(1);
    auto cosf1 = [](double x) { return std::cos(x); };
    auto rig = RombergIntegration(cosf1, PI, 0);
    std::cout << rig.first << " " << rig.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
        const double PI = 4 * std::atan(1);
    auto cosf1 = [](double x) { return std::cos(x); };
    auto rig = RombergIntegration(cosf1, PI, 0, 1e-6, 1000, 1000000);
    std::cout << rig.first << " " << rig.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
        const double PI = 4 * std::atan(1);
    auto cosf1 = [](double x) { return std::cos(x); };
    auto rig = RombergIntegration(cosf1, PI, 0, -1e-6, 1000, 1000000);
    std::cout << rig.first << " " << rig.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
        auto fja = [](double x) {  return x == 0 ? 0 : 1/(x*x); };
    auto rig = RombergIntegration(fja, 0, 1);
    std::cout << rig.first << " " << rig.second<< std::endl;
    rig = RombergIntegration(fja, 0, 1, 1e-8, 100000);
    std::cout << rig.first << " " << rig.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    std::cout<<"TanhSinhIntegration"<<std::endl;
    try{
      auto rez = TanhSinhIntegration([](double x) { return log(fabs(x - 1)); }, 0, 1, -1e-8, 1000000);
    std::cout << rez.first << " " << rez.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }try{
      auto rez = TanhSinhIntegration([](double x) { return log(fabs(1-x)); }, 0, 1, 1e-8, 10000000);
    std::cout << rez.first << " " << rez.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }try{
      auto cosc = [](double x) { return x == 0 ? 1 : 1/std::cos(x); };
    auto rez = TanhSinhIntegration(cosc, 0, 1);
    std::cout << rez.first << " " << rez.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }try{
      auto cosc = [](double x) { return x == 0 ? 1 : std::cos(x) / x; };
    auto rez = TanhSinhIntegration(cosc, 0, 1, 1e-10, -10000);
    std::cout << rez.first << " " << rez.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    std::cout<<"AdaptiveIntegration"<<std::endl;
    try{
      auto cosc = [](double x) { return x == 0 ? 1 : 1/std::cos(x); };
    auto rez = AdaptiveIntegration(cosc, 0, 1);
    std::cout << rez.first << " " << rez.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
      auto cosc = [](double x) { return x == 0 ? 1 : std::cos(x) / x; };
    auto rez = AdaptiveIntegration(cosc, 0, 1, 1e-10, -10000);
    std::cout << rez.first << " " << rez.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
      const double PI = 4 * std::atan(1);
    auto cosf1 = [](double x) { return std::cos(x); };
    auto rig = AdaptiveIntegration(cosf1, PI, 0);
    std::cout << rig.first << " " << rig.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
      const double PI = 4 * std::atan(1);
    auto cosf1 = [](double x) { return std::cos(x); };
    auto rig = AdaptiveIntegration(cosf1, PI, 0, 1e-12, 50, 60);
    std::cout << rig.first << " " << rig.second<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    std::cout<<"ChebyshevApproximation"<<std::endl;
    try{
      const double PI = 4 * std::atan(1);
    auto funcos = [](double x) { return std::cos(x); };
    ChebyshevApproximation cosch(funcos, 0, PI, 10);
    std::cout << funcos(1) << " " << cosch(1)<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }try{
      const double PI = 4 * std::atan(1);
    auto funcos = [](double x) { return std::cos(x); };
    ChebyshevApproximation cosch(funcos, PI, 0, 10 );
    std::cout << funcos(1) << " " << cosch(1)<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }try{
      const double PI = 4 * std::atan(1);
    auto fun = [](double x) { return std::cos(x); };
    ChebyshevApproximation cosch(fun, 0, PI, 20);
    std::cout << fun(2) << " " << cosch(2)<<std::endl;
    }
    catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
	return 0;
}