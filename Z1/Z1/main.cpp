//NA 2017/2018: Zadaća 1, Zadatak 1
#include <iostream>
#include <cmath>
#include <vector>
#include <initializer_list>
#include <stdexcept>
#include <limits>
#include <iomanip>

typedef std::vector<std::vector<double>> Matrica;

class Vector{
    private:
    std::vector<double> vek;
    public:
    explicit Vector(int n){
        if(n<=0) throw std::range_error("Bad dimension");
        for(int i=0;i<n;i++)
        vek.push_back(0);
    }
    
    Vector(std::initializer_list<double> l){
        if(l.size()<=0) throw std::range_error("Bad dimension");
        for(auto it=l.begin();it!=l.end();it++) vek.push_back(*it);
    }
    
    int NElems() const{
        return vek.size();
    }
    
    double &operator[](int i){
        return vek[i];
    }
    
    double operator[](int i) const{
        return vek[i];
    }
    
    double &operator()(int i){
        if(i<1 || i>vek.size()) throw std::range_error("Invalid index");
        return vek[i-1];
    }
    
    double operator()(int i) const{
        if(i<1 || i>vek.size()) throw std::range_error("Invalid index");
        return vek[i-1];
    }
    
    double Norm() const{
        double suma(0);
        for(int i=0;i<NElems();i++)
        suma+=pow(vek[i],2);
        return sqrt(suma);
    }
    
    friend double VectorNorm(const Vector &v);
    
    double GetEpsilon() const{
        auto ep(std::numeric_limits<double>::epsilon());
        return ep*Norm();
    }
    
    void Print(char separator='\n', double eps=-1)const{
        for(int i=0;i<NElems();i++){
            if(fabs(vek[i]-GetEpsilon())<eps) std::cout<<"0";
            else std::cout<<vek[i];
            if(i!=NElems()-1) std::cout<<separator;
        }
    }
    
    friend void PrintVector(const Vector &v, char separator, double eps);
    
    friend Vector operator+(const Vector &v1, const Vector &v2);
    
    Vector &operator+=(const Vector &v){
        if(NElems()!=v.NElems()) throw std::domain_error("Incompatible formats");
        for(int i=0;i<NElems();i++)
        vek[i]+=v[i];
        return *this;
    }
    
    friend Vector operator-(const Vector &v1, const Vector &v2);
    
    Vector &operator-=(const Vector &v){
        if(NElems()!=v.NElems()) throw std::domain_error("Incompatible formats");
        for(int i=0;i<NElems();i++)
        vek[i]-=v[i];
        return *this;
    }
    
    friend Vector operator*(double s, const Vector &v);
    
    friend Vector operator*(const Vector &v, double s);
    
    Vector &operator*=(double s){
        for(int i=0;i<NElems();i++)
        vek[i]*=s;
        return *this;
    }
    
    friend double operator*(const Vector &v1, const Vector &v2);
    
    friend Vector operator/(const Vector &v, double s);
    
    Vector &operator/=(double s){
        if(fabs(s-0)<GetEpsilon()) throw std::domain_error("Division by zero");
        for(int i=0;i<NElems();i++)
        vek[i]/=s;
        return *this;
    }
};

double VectorNorm(const Vector &v){
    double suma(0);
        for(int i=0;i<v.NElems();i++)
        suma+=pow(v[i],2);
        return sqrt(suma);
}

void PrintVector(const Vector &v, char separator='\n', double eps=-1){
    for(int i=0;i<v.NElems();i++){
            if(fabs(v[i]-v.GetEpsilon())<eps) std::cout<<"0";
            else std::cout<<v[i];
            if(i!=v.NElems()-1 && separator!='\n') std::cout<<separator;
            else std::cout<<separator;
            
        }
}

Vector operator+(const Vector &v1, const Vector &v2){
    if(v1.NElems()!=v2.NElems()) throw std::domain_error("Incompatible formats");
    Vector rezultat(v1);
    return rezultat+=v2;
}

Vector operator-(const Vector &v1, const Vector &v2){
    if(v1.NElems()!=v2.NElems()) throw std::domain_error("Incompatible formats");
    Vector rezultat(v1);
    return rezultat-=v2;
}

Vector operator*(double s, const Vector &v){
    Vector rezultat(v);
    for(int i=0;i<v.NElems();i++)
    rezultat[i]*=s;
    return rezultat;
}

Vector operator*(const Vector &v, double s){
    Vector rezultat(v);
    for(int i=0;i<v.NElems();i++)
    rezultat[i]*=s;
    return rezultat;
}

double operator*(const Vector &v1, const Vector &v2){
    if(v1.NElems()!=v2.NElems()) throw std::domain_error("Incompatible formats");
    double rezultat(0);
    for(int i=0;i<v1.NElems();i++)
    rezultat+=v1[i]*v2[i];
    return rezultat;
}

Vector operator/(const Vector &v, double s){
    if(fabs(s-0)<v.GetEpsilon()) throw std::domain_error("Division by zero");
    Vector rezultat(v);
    for(int i=0;i<rezultat.NElems();i++)
    rezultat[i]/=s;
    return rezultat;
}

class Matrix{
    private:
    Matrica M;
    public:
    Matrix(int m, int n){
        if(m<1 || n<1) throw std::range_error("Bad dimension");
        M.resize(m);
        for(int i=0;i<m;i++)
            M[i].resize(n);
    }
    
    Matrix(const Vector &v){
        M.resize(1);
        for(int i=0;i<v.NElems();i++)
            for(int j=0;j<1;j++)
                M[0].push_back(v[i]);
    }
    
    Matrix(std::initializer_list<std::vector<double>> l){
        auto tmp(l.begin());
        int br_el(tmp->size());
        if(l.begin()==l.end()) throw std::range_error("Bad dimension");
        while(tmp!=l.end()){
            if(tmp->size()==0) throw std::range_error("Bad dimension");
            if(tmp->size()!=br_el) throw std::logic_error("Bad matrix");
            tmp++;
        }
        tmp=l.begin(); int brojac(0);
        while(tmp!=l.end()){
            brojac++; tmp++;
        }
        tmp=l.begin();
        M.resize(brojac);
        int i(0);
        while (tmp!=l.end()) {
            M[i]=*tmp; i++; tmp++;
        }
    }
    
    int NRows() const{
        return M.size();
    }
    
    int NCols() const{
        return M[0].size();
    }
    
    double *operator[](int i){
        return &M[i][0];
    }
    
    const double *operator[](int i) const{
        return &M[i][0];
    }
    
    double &operator()(int i, int j){
        if(i<1 || i>NRows() || j<1 || j>NCols()) throw std::range_error("Invalid index");
        return M[i-1][j-1];
    }
    
    double operator ()(int i, int j) const{
        if(i<1 || i>NRows() || j<1 || j>NCols()) throw std::range_error("Invalid index");
        return M[i-1][j-1];
    }
    
    double Norm() const{
        double suma(0);
        for(int i=0;i<NRows();i++)
            for(int j=0;j<NCols();j++)
            suma+=pow(M[i][j],2);
            return sqrt(suma);
    }
    
    friend double MatrixNorm(const Matrix &m); //nap
    
    double GetEpsilon() const{
        auto ep(std::numeric_limits<double>::epsilon());
        return ep*Norm();
    }
    
    void Print(int width=10, double eps=-1)const{
        for(int i=0;i<NRows();i++){
            for(int j=0;j<NCols();j++){
                if(fabs(M[i][j]-GetEpsilon())<eps) std::cout<<std::setw(width)<<"0";
                else std::cout<<std::setw(width)<<M[i][j];
            }
            std::cout<<std::endl;
        }
    } //provjeri
    
    friend void PrintMatrix(const Matrix &m, int width, double eps); //provjeri
    
    friend Matrix operator+(const Matrix &m1, const Matrix &m2); //nap
    
    Matrix &operator+=(const Matrix &m){
        if(NRows()!=m.NRows() || NCols()!=m.NCols()) throw std::domain_error("Incompatible formats");
        for(int i=0;i<NRows();i++)
            for(int j=0;j<NCols();j++)
                M[i][j]+=m[i][j];
        return *this;
    } //nap
    
    friend Matrix operator-(const Matrix &m1, const Matrix &m2); //nap
    
    Matrix &operator-=(const Matrix &m){
        if(NRows()!=m.NRows() || NCols()!=m.NCols()) throw std::domain_error("Incompatible formats");
        for(int i=0;i<NRows();i++)
            for(int j=0;j<NCols();j++)
                M[i][j]-=m[i][j];
        return *this;
    } //nap
    
    friend Matrix operator*(double s, const Matrix &m); //nap
    
    friend Matrix operator*(const Matrix &m, double s); //nap
    
    Matrix &operator*=(double s){
        for(int i=0;i<NRows();i++)
            for(int j=0;j<NCols();j++)
                M[i][j]*=s;
        return *this;
    }
    
    friend Matrix operator*(const Matrix &m1, const Matrix &m2); //nap
    
    Matrix &operator*=(const Matrix &m){
        if(NCols()!=m.NRows()) throw std::domain_error("Incompatible formats");
    Matrix rezultat(NRows(),m.NCols());
    for(int i=0; i<NRows();i++)
        for(int j=0;j<m.NCols();j++)
            for(int k=0;k<NCols();k++)
                rezultat[i][j]+=M[i][k]*m[k][j];
    return *this=rezultat;
    }
    
    friend Vector operator*(const Matrix &m, const Vector &v); //nap
    
    friend Matrix Transpose(const Matrix &m); //nap
    
    void Transpose(){
        if(NRows()==NCols()){
            for(int i=0;i<NCols();i++){
                for(int j=i;j<NRows();j++){
                    auto tmp(M[i][j]);
                    M[i][j]=M[j][i];
                    M[j][i]=tmp;
                }
            }
        }
        else{
            Matrix tmp(NCols(),NRows());
            for(int i=0;i<NCols();i++)
                for(int j=0;j<NRows();j++)
                tmp[i][j]=M[j][i];
            *this=tmp;
        }
    } //nap
};

double MatrixNorm(const Matrix &m){
    double suma(0);
    for(int i=0;i<m.NRows();i++)
    for(int j=0;j<m.NCols();j++)
    suma+=pow(m[i][j],2);
    return sqrt(suma);
}

void PrintMatrix(const Matrix &m, int width=10, double eps=-1){
        for(int i=0;i<m.NRows();i++){
            for(int j=0;j<m.NCols();j++){
                if(fabs(m[i][j]-m.GetEpsilon())<eps) std::cout<<std::setw(width)<<"0";
                else std::cout<<std::setw(width)<<m[i][j];
            }
            std::cout<<std::endl;
        }
    }

Matrix operator+(const Matrix &m1, const Matrix &m2){
    if(m1.NRows()!=m2.NRows() || m1.NCols()!=m2.NCols()) throw std::domain_error("Incompatible formats");
    Matrix rezultat(m1.NRows(),m1.NCols());
    for(int i=0;i<rezultat.NRows();i++)
        for(int j=0;j<rezultat.NCols();j++)
            rezultat[i][j]=m1[i][j]+m2[i][j];
    return rezultat;
}

Matrix operator-(const Matrix &m1, const Matrix &m2){
    if(m1.NRows()!=m2.NRows() || m1.NCols()!=m2.NCols()) throw std::domain_error("Incompatible formats");
    Matrix rezultat(m1.NRows(),m1.NCols());
    for(int i=0;i<rezultat.NRows();i++)
        for(int j=0;j<rezultat.NCols();j++)
            rezultat[i][j]=m1[i][j]-m2[i][j];
    return rezultat;
}

Matrix operator*(double s, const Matrix &m){
    Matrix rezultat(m.NRows(),m.NCols());
    for(int i=0;i<m.NRows();i++)
        for(int j=0;j<m.NCols();j++)
            rezultat[i][j]=m[i][j]*s;
    return rezultat;
}

Matrix operator*(const Matrix &m, double s){
     Matrix rezultat(m.NRows(),m.NCols());
    for(int i=0;i<m.NRows();i++)
        for(int j=0;j<m.NCols();j++)
            rezultat[i][j]=m[i][j]*s;
    return rezultat;
}

Matrix operator*(const Matrix &m1, const Matrix &m2){
    if(m1.NCols()!=m2.NRows()) throw std::domain_error("Incompatible formats");
    Matrix rezultat(m1.NRows(),m2.NCols());
    for(int i=0; i<m1.NRows();i++)
        for(int j=0;j<m2.NCols();j++)
            for(int k=0;k<m1.NCols();k++)
                rezultat[i][j]+=m1[i][k]*m2[k][j];
    return rezultat;
}

Vector operator*(const Matrix &m, const Vector &v){
    if(m.NCols()!=v.NElems()) throw std::domain_error("Incompatible formats");
    Vector rezultat(m.NRows());
    for(int i=0;i<m.NRows();i++)
        for(int j=0;j<m.NCols();j++)
            rezultat[i]+=m[i][j]*v[j];
    return rezultat;
}

Matrix Transpose(const Matrix &m){
        Matrix tmp(m.NCols(),m.NRows());
            for(int i=0;i<m.NCols();i++)
                for(int j=0;j<m.NRows();j++)
                    tmp[i][j]=m[j][i];
        return tmp;
}

void TVek_Konstr1(){
    Vector v(-6);
    v.Print();
}

int main (){
    Vector v1(10);
    v1.Print();
    try {
        Vector v2 (-8767);
    } catch (std::range_error izuzetak) {
        std::cout << izuzetak.what() << std::endl;
    } 
    try {
        Vector v3 {};
    } catch (std::range_error izuzetak) {
        std::cout <<izuzetak.what() << std::endl;
    } 
    Vector v4 {7.55,3.39,16.87,876.33,0.297};
    v4.Print('*');
    Vector v5(1000000);
    std::cout<<v5.NElems();
    Vector v6 {1.45,2.65,3.654,4.865,5.443,6.63,7.002};
    v6[3]=123.456;
    std::cout<<v6[3]<<std::endl; std::cout<<v6(3)<<std::endl;
    Vector v7 {1.34,3.45,56.6}, v8 {45.654,64.54,90.878};
    (v7+v8).Print('+');
    std::cout<<std::endl;
    (v8-v7).Print('-');
   
    try {
        Vector v11 {876.987,987.23,97.76,987.34,234.876}, v12 {123.987,87.876,34.787};
        Vector rezultat2(v11-v12);
    } catch (std::domain_error izuzetak) {
        std::cout <<izuzetak.what() << std::endl;
    }
    Vector v13 {9876,456,876,3456,9876,44589,929383}, v14(7);
    v13+=v14;
    v13.Print();
    v14-=v13;
    std::cout<<std::endl;
    v14.Print();
    try {
        Vector v15 {1,2,3}, v16 {1,2,3,4,5,6,7,8,9};
        v15+=v16;
    } catch (std::domain_error izuzetak) {
        std::cout <<izuzetak.what() << std::endl;
    } 
    try {
        Vector v17 {1,2,3}, v18 {1,2,3,4,5,6,7,8,9};
        v18-=v17;
    } catch (std::domain_error izuzetak) {
        std::cout <<izuzetak.what() << std::endl;
    } 
    Vector v19 {9876.987,987.567,232.987,876.213};
    v19.Print(' ');
    v19=v19/100;
    std::cout<<std::endl;
    v19=100*v19;
    std::cout<<std::endl;
    v19.Print(' ');
    Vector v20 {9876,456,876,3456,9876,44589,929383};
    v20*=100;
    v20.Print(' ');
    v20/=100000;
    std::cout<<std::endl;
    v20.Print(' ');
    Vector v21 {1,3,5,7,9}, v22 {2,4,6,8,10};
    std::cout<<v21*v22;
    try {
        Vector v23 {1,2,3}, v24 {1,2};
        double a=v23*v24;
        std::cout<<a<<std::endl;
    } catch (std::domain_error izuzetak) {
        std::cout <<izuzetak.what() << std::endl;
    } 
    try {
        Matrix mat1 (0,1000);
    } catch (std::range_error izuzetak) {
        std::cout <<izuzetak.what() << std::endl;
    } 
    Matrix mat2(3,2), mat3 {{1.25,2.34},{3.87,4.12},{5.123,5.66}};
    mat2+=mat3;
    mat2.Print(10);
    mat2-=mat3;
    std::cout<<std::endl;
    mat2.Print(20);
    try {
        Matrix mat4 {{7.5,2.64},{7.77,2.11},{5.12,5.226}},mat5 {{1,2},{2,3}};
        mat4-=mat5;
    } catch (std::domain_error izuzetak) {
        std::cout <<izuzetak.what() << std::endl;
    } 
    Matrix mat6 {{1.25,2.34},{3.87,4.12},{5.123,5.66}};
    
    
    mat6=2*mat2;
    std::cout<<std::endl;
    mat6.Print(6);
    mat6*=10;
    std::cout<<std::endl;
    mat2.Print(6);
    Vector v {0.325, -2.345, 16.007};
    PrintVector(v);
    std::cout << std::endl;
    PrintVector(v, ' ');
    std::cout << std::endl ;
    PrintVector(v, ' ', 1);
    const Matrix m {
        {0.325, -1.345, 16.007}, {2.35, 6.54, -6.124}
    };
    m.Print(9);
    std::cout << std::endl;
    m.Print(11, 2);
    Matrix m12 {{0.325, -1.345, 16.007}, {2.35, 6.54, -6.124}};
    Matrix m13 {{0.325, 1.345, 6.007}, {2.35, 6.444, -6.124}};
    Matrix m14 {{1.325, 1.344, 16.007}, {1.35, 6.74, 6.124}};
    Matrix m11 {{0.325, -1.345, 1.7}, {-2.35, 6.54, -6.124}};
    std::cout << MatrixNorm(m11) << " " << MatrixNorm(m12) << " " << MatrixNorm(m13) << " " << MatrixNorm(m14);
	return 0;
}