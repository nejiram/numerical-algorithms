//NA 2017/2018: Zadaća 5, Zadatak 1
#include <iostream>
#include <cmath>
#include <stdexcept>
#include <vector>
#include <complex>
#include <algorithm>
#include <limits>
#include <utility>

//pomocne funkcije

int sgn(double x){
    if (x > 0) return 1;
    if (x < 0) return -1;
    return 0;
}

double FI(double x){
    return (x/(1+fabs(x))); 
}

template <typename FunTip>
double RK4Step(FunTip f, double x, double y, double h){
    std::vector<double> k(4);
    k[0]=f(x, y);
    k[1]=f(x+h/2, y+h*k[0]/2);
    k[2]=f(x+h/2, y+h *k[1]/2);
    k[3]=f(x+h, y+h*k[2]);
    return y+h*(k[0]+2*k[1]+2*k[2]+k[3])/6;
}

template <typename FunTip>
void RK4Adaptive(FunTip f, double x0, double y0, double xmax, double h, std::vector<std::pair<double, double>> &vek, double eps = 1e-8){
    auto x(x0), y(y0);
    vek.push_back(std::pair<double, double>(x, y));
    while((h>0 && x<=xmax) || (h<0 && x >=xmax)) {
        double u(RK4Step(f, x, y, h/2)), v(RK4Step(f, x+h/2, u, h/2)), w(RK4Step(f, x, y, h));
        double delta(std::fabs(w-v)/h);
        if(delta<=eps) {
            x=x+h;
            y=v;
            vek.push_back(std::pair<double, double>(x, y));
        }
        double pom(0.9*std::pow(eps/delta, 1/4));
        //double pom(0.9 * h * std::sqrt(std::sqrt(eps/delta)));
        if(pom<5) h*=pom;
        else h*= 5;
    }
}

template <typename FunTip>
void RK4NoAdaptive(FunTip f, double x0, double y0, double xmax, double h, std::vector<std::pair<double, double>> &vek, double eps = 1e-8){
    double x(x0), y(y0);
    std::vector<double> k(4);
    if((h>0 && x>xmax+eps) || (h<0 && x<xmax+eps)) {
        vek.push_back(std::pair<double, double>(x, y));
        return;
    }
    while((h>0 && x<=xmax+h/1000) || (h<0 && x>=xmax+h/1000)) {
        vek.push_back(std::pair<double, double>(x, y));
        k[0]=f(x, y);
        k[1]=f(x+h/2, y+ h*k[0]/2);
        k[2]=f(x+h/2, y+ h*k[1]/2);
        k[3]=f(x+h, y+h*k[2]);
        y=y+h*(k[0]+2*k[1]+2*k[2]+k[3])/6;
        x+=h;
    }
}

std::complex<double> Laguerre(std::vector<std::complex<double>> p, int n, std::complex<double> x, bool& c, double eps = 1e-10, double maxiter = 100) {
	std::complex<double> deltax(std::numeric_limits<double>::infinity());
	int k(1);
	while (abs(deltax)>eps && k<maxiter) {
		std::complex<double> f(p[n]), d(0), s(0);
		for (int i=n-1; i>=0; i--) { 
			s=s*x+2.0*d;
			d=d*x+f;
			f=f*x+p[i];
		}

		if (fabs(x)<=eps) {
			c=true;
			return x;
		}
		auto r(sqrt(double(n-1)*(double(n-1)*d*d-double(n)*f*s)));
		if (fabs(d+r)>fabs(d-r))
			deltax=double(n)*f/(d+r);
		else
			deltax=double(n)*f/(d-r);
		x-=deltax;
		k++;
	}
	if (fabs(deltax)<=eps) {
		c=true;
		return x;
	}
	c=false;
	return x;
}

std::complex<double> Laguerre(std::vector<double> p, int n, std::complex<double> x,	bool& c, double eps = 1e-10, double maxiter = 100) {
	std::vector<std::complex<double>> p2(p.size());
	for (int i=0; i<p2.size(); i++)
		p2[i]=std::complex<double>(p[i]);
	return Laguerre(p2, n, x, c, eps, maxiter);
}

double fRand(double fMin, double fMax){
	double f=(double)rand()/RAND_MAX;
	return fMin+f*(fMax-fMin);
}

std::complex<double> RandomComplex(double rmin, double rmax, double imin, double imax) {
	return std::complex<double>(fRand(rmin, rmax), fRand(imin, imax));
}

template <typename FunTip>
double BracketMinimum(FunTip f, double x0, double& xfinal,	double eps = 1e-8, double hinit = 1e-5, double hmax = 1e10,	double lambda = 1.4) {
	double h(hinit), x(x0);
	while (abs(h)<hmax) {
		if (f(x-h)<f(x))
			h*=-1;
		else if (!(f(x+h)<f(x))) {
			xfinal=x;
			if(h<0) return -h;
			return h;
		}
		x+=h;
		h*=lambda;
	}
	throw std::logic_error("Minimum has not found");
}

//trazene funkcije

enum RegulaFalsiMode {Unmodified, Illinois, Slavic, IllinoisSlavic};

template <typename FunTip>
bool BracketRoot(FunTip f, double x0, double &a, double &b, double hinit = 1e-5, double hmax = 1e10, double lambda = 1.4){
    if(hinit<0 || hmax<0 || lambda<0) throw std::domain_error("Invalid parameters");
    double pomhinit(hinit);
    a=x0;
    auto f1(f(a));
    while(fabs(hinit)<hmax){
        b=a+hinit;
        auto f2(f(b));
        if(f1*f2<=0){
            return true;
        }
        hinit*=lambda;
        a=b;
        f1=f2;
    }
    //drugi pokusaj
    hinit=pomhinit;
    a=x0;
    f1=f(a);
    hinit*=-1;
    while(fabs(hinit)<hmax){
        b=a+hinit;
        auto f2(f(b));
        if(f1*f2<=0){
            if(a>b){ //uvijek mora biti b>a
                auto tmp(a);
                a=b;
                b=tmp;
            }
            return true;
        }
        hinit*=lambda;
        a=b;
        f1=f2;
    }
    return false;
}

template <typename FunTip>
double RegulaFalsiSolve(FunTip f, double a, double b, RegulaFalsiMode mode = Slavic, double eps = 1e-10, int maxiter = 100){
    if((f(a)<0 && f(b)<0)||(f(a)>0 && f(b)>0)) throw std::range_error("Root must be bracketed");
    if(eps<=0 || maxiter<=0) throw std::domain_error("Invalid parameters");
    if(mode==Unmodified){
        int i(0);
        double f1(f(a)), f2(f(b));
        double c(a), cold(b), f3;
        while(fabs(c-cold)>eps){
            i++;
            cold=c;
            c=(a*f2-b*f1)/(f2-f1);
            f3=f(c);
            if(fabs(f3-0)<eps) {
                if(i>maxiter) throw std::logic_error("Given accuracy has not achieved");
                return c;
            }
            if(f1*f3<0){
                b=c;
                f2=f3;
            }
            else{
                a=c;
                f1=f3;
            }
        }
        if(i>maxiter) throw std::logic_error("Given accuracy has not achieved");
        return c;
    }
    else if(mode==Illinois){
        double f1(f(a)), f2(f(b));
        double c(a), cold(b), f3;
        int i(0);
        while(fabs(c-cold)>eps){
            i++;
            cold=c;
            c=(a*f2-b*f1)/(f2-f1);
            f3=f(c);
            if(fabs(f3-0)<eps) {
                if(i>maxiter)throw std::logic_error("Given accuracy has not achieved");
                return c;
            }
            if(f1*f3<0){
                b=a;
                f2=f1;
            }
            else{
                f2=f2/2;
            }
            a=c;
            f1=f3;
        }
        if(i>maxiter)throw std::logic_error("Given accuracy has not achieved");
        return c;
    }
    else if(mode==Slavic){
        double f1(FI(f(a))), f2(FI(f(b)));
        double c(a), cold(b), f3;
        int i(0);
        while(fabs(c-cold)>eps){
            i++;
            cold=c;
            c=(a*f2-b*f1)/(f2-f1);
            f3=FI(f(c));
            if(fabs(f3-0)<eps) {
                if(i>maxiter) throw std::logic_error("Given accuracy has not achieved");
                return c;
            }
            if(f1*f3<0){
                b=a;
                f2=f1;
            }
            a=c;
            f1=f3;
        }
        if(i>maxiter) throw std::logic_error("Given accuracy has not achieved");
        return c;
    }
    else if(mode==IllinoisSlavic){
        double f1(FI(f(a))), f2(FI(f(b)));
        double c(a), cold(b), f3;
        int i(0);
        while(fabs(c-cold)>eps){
            i++;
            cold=c;
            c=(a*f2-b*f1)/(f2-f1);
            f3=FI(f(c));
            if(fabs(f3-0)<eps) {
                if(i>maxiter)throw std::logic_error("Given accuracy has not achieved");
                return c;
            }
            if(f1*f3<0){
                b=a;
                f2=f1;
            }
            else{
                f2=f2/2;
            }
            a=c;
            f1=f3;
        }
        if(i>maxiter)throw std::logic_error("Given accuracy has not achieved");
        return c;
    }
}

template <typename FunTip>
double RiddersSolve(FunTip f, double a, double b, double eps = 1e-10, int maxiter = 100){
    if((f(a)<0 && f(b)<0)||(f(a)>0 && f(b)>0)) throw std::range_error("Root must be bracketed");
    if(eps<=0 || maxiter<=0) throw std::domain_error("Invalid parameters");
    auto f1(f(a));
    auto f2(f(b));
    int i(0);
    while(fabs(b-a)>eps){
        auto c((a+b)/2);
        auto f3(f(c));
        if(fabs(f3-0)<eps){
            if(!(i<maxiter))throw std::logic_error("Given accuracy has not achieved");
            return c;
        }
        auto d(c+f3*(c-a)*sgn(f1-f2)/(sqrt(f3*f3-f1*f2)));
        auto f4(f(d));
        if(fabs(f4-0)<eps){
            if(!(i<maxiter))throw std::logic_error("Given accuracy has not achieved");
            return d;
        }
        if(f3*f4<=0){
            a=c;
            b=d;
            f1=f3;
            f2=f4;
        }
        else if(f1*f4<=0){
            b=d;
            f2=f4;
        }
        else{
            a=d;
            f1=f4;
        }
        i++;
    }
    if(!(i<maxiter))throw std::logic_error("Given accuracy has not achieved");
    return (a+b)/2;
}

template <typename FunTip1, typename FunTip2>
double NewtonRaphsonSolve(FunTip1 f, FunTip2 fprim, double x0, double eps = 1e-10, int maxiter = 100){
    if(eps<=0 || maxiter<=0) throw std::domain_error("Invalid parameters");
    auto deltax(std::numeric_limits<double>::infinity());
    int i(0);
    while(fabs(deltax)>eps){
        auto v(f(x0));
        if(fabs(v-0)<eps){
            if(!(i<maxiter))throw std::logic_error("Convergence has not achieved");
            return x0;
        }
        deltax=v/(fprim(x0));
        if(fabs(v-std::numeric_limits<double>::infinity())<eps || fabs(fprim(x0)-0)<eps)
        throw std::logic_error("Convergence has not achieved");
        x0=x0-deltax;
        i++;
    }
    if(!(i<maxiter)) throw std::logic_error("Convergence has not achieved");
    return x0;
}

std::vector<std::complex<double>>PolyRoots( std::vector<std::complex<double>> coefficients, double eps = 1e-10, int maxiters = 100, int maxtrials = 10){
    if(eps<=0 || maxiters<=0 || maxtrials<=0) throw std::domain_error("Invalid parameters");
    
    
    
    std::vector<std::complex<double>> roots(coefficients.size()),	porig(coefficients);
	for (int i=int(coefficients.size())-1; i>=1; i--) {
		int t(1);
		bool c(false);
		std::complex<double> x;
		while(!c && t<maxtrials) {
			x=Laguerre(coefficients, i, RandomComplex(-10, 10, -10, 10), c, eps, maxiters);
			t++;
		}
		if(c==false)	throw std::logic_error("Convergence has not achieved");
		//Poliranje
		auto xpol(Laguerre(porig, int(porig.size()) - 1, x, c, eps, maxiters));
		if(c==true)	x = xpol;
		if (abs(x.imag())<=eps)	x=real(x);
		if (abs(x.real())<=eps)	x={0, x.imag()};
		roots[i]=x;
		auto v(coefficients[i]);
		for (int j=i-1; j>=0; j--) {
			auto w(coefficients[j]);
			coefficients[j]=v;
			v=w+x*v;
		}
	}
	roots.erase(roots.begin());
	return roots;
    
    
    
}
 
std::vector< std::complex<double>> PolyRoots( std::vector<double> coefficients, double eps = 1e-10, int maxiters = 100, int maxtrials = 10){
    if(eps<=0 || maxiters<=0 || maxtrials<=0) throw std::domain_error("Invalid parameters"); 
    std::vector<std::complex<double>> roots(coefficients.size());
	std::vector<double> porig(coefficients);
	int i(coefficients.size() - 1);
	while (i>=1) {
		int t(1);
		bool c(false);
		std::complex<double> x;
		while(!c && (t<maxtrials)) {
			x=Laguerre(coefficients, i, RandomComplex(-10, 10, -10, 10), c, eps, maxiters);
			t++;
		}
		if (c==false) throw std::logic_error("Convergence has not achieved");

		//Poliranje
		auto xpol(Laguerre(porig, porig.size() - 1, x, c, eps, maxiters));
		if (c==true)
			x = xpol;
		if (fabs(x.imag())<=eps) {
			x=x.real();
			roots[i]=x;
			double v(coefficients[i]);
			for (int j=i-1; j>=0; j--) {
				double w(coefficients[j]);
				coefficients[j]=v;
				v=w+x.real()*v;
			}
			i--;
		}
		else {
			if (fabs(x.real())<=eps) {
				x={0, x.imag()};
			}
			roots[i]=x;
			roots[i-1]=std::conj(x);
			auto alpha(2*x.real());
			auto beta(abs(x)*abs(x));
			auto u(coefficients[i]);
			auto v(coefficients[i-1]+alpha*u);
			for (int j=i-2; j>=0; j--) {
				auto w(coefficients[j]);
				coefficients[j]=u;
				u=v;
				v=w+alpha*v-beta*coefficients[j];
			}
			i -= 2;
		}
	}
	roots.erase(roots.begin());
	return roots;
}
 
template <typename FunTip> 
double FindMinimum(FunTip f, double x0, double eps = 1e-8, double hinit = 1e-5, double hmax = 1e10, double lambda = 1.4){
    if(hinit<0 || hmax<0 || lambda<0 || eps<0) throw std::domain_error("Invalid parameters");
    double xfinal, hfinal(BracketMinimum(f, x0, xfinal, eps, hinit, hmax, lambda));
	double a(xfinal - hfinal), c(xfinal), b(xfinal + hfinal), d;
	double R(0.61803399), C(1.0 - R);
	double x1, x2, x(a), x3(b);
	if(abs(b-c)>abs(c-a)) {
		x1=c;
		x2=c+C*(b-c);
	}
	else{
		x2=c;
		x1=c-C*(c-a);
	}
	double f1(f(x1)), f2(f(x2));
	while (fabs(x0-x3)>eps){
		if(f2<f1){
			x=x1;
			x1=x2;
			x2=R*x2+C*x3;
			f1=f2;
			f2=f(x2);
		}
		else {
			x3=x2;
			x2=x1;
			x1=R*x1+C*x;
			f2=f1;
			f1=f(x1);
		}
	}
	return (x+x3)/2;
}
 
template <typename FunTip>
std::vector<std::pair<double, double>> RK4Integrator(FunTip f, double x0, double y0, double xmax, double h, double eps = 1e-8, bool adaptive = false){
    std::vector<std::pair<double, double>> v;
    if(adaptive==true)
        RK4Adaptive(f, x0, y0, xmax, h, v, eps);
    else
        RK4NoAdaptive(f, x0, y0, xmax, h, v, eps);
    return v;
}

std::vector<double> LossyCompress(std::vector<double> data, int new_size){
    int N(int(data.size()));
    if(!(new_size>1 && new_size<=N)) throw std::range_error("Bad new size");
    bool stepen(false);
    for(int i=0; i<20; i++){
        if(pow(2,i)==N){
            stepen=true;
            break;
        }
    }
    if(stepen==false) throw std::range_error("Data size must be a power of two");
}

std::vector<double> LossyDecompress(std::vector<double> compressed){
    int N=int(compressed.size());
    if (N<0 || N<compressed.size()) throw std::logic_error("Bad compressed sequence");
    bool stepen(false);
    for(int i=0; i<20; i++){
        if(pow(2,i)==N){
            stepen=true;
            break;
        }
    }
    if(stepen==false) throw std::range_error("Data size must be a power of two");
}


int main (){
    std::cout<<"RADI"<<std::endl;

    double a,b;
    try{
    bool s=BracketRoot([](double x) { return x + 19; }, 0, a, b);
    std::cout<<s<<std::endl;
    bool t=BracketRoot([](double x) { return x + 19; }, 0, a, b, -123);
    } catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
    std::cout<<RegulaFalsiSolve([](double x) { return x/2; }, 4, -2)<<std::endl;
    std::cout<<RegulaFalsiSolve([](double x) { return x/2; }, 4, -2, IllinoisSlavic ,-123)<<std::endl;
    } catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
    std::cout<<RiddersSolve([](double x) { return x/9; }, 4, -5)<<std::endl;
    std::cout<<RiddersSolve([](double x) { return x/9; }, -4, -5)<<std::endl;
    std::cout<<RiddersSolve([](double x) { return x/9; }, 4, -5, -1e-5)<<std::endl;
    } catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }catch(std::logic_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }catch(std::range_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
    std::cout<<NewtonRaphsonSolve([](double x) { return x*x; }, [](double x) { return 2*x; }, 2)<<std::endl;
    std::cout<<NewtonRaphsonSolve([](double x) { return x*x; }, [](double x) { return 2*x; }, 2, -1e-20)<<std::endl;
    }catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }catch(std::logic_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    std::vector<double> vek = { 0.5, 1.5, -2.5 };
    try{
    auto vek1 = LossyDecompress(vek);
    } catch(std::range_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
    auto vek2 = LossyCompress(vek, 9);
    }catch(std::range_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
    auto vek3 = PolyRoots(vek);
    } catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
    try{
    std::cout<<FindMinimum([](double x) { return x*x; }, 2.5, 1e-8, 1e-10, 1e3, -2.5)<<std::endl;
    }catch(std::domain_error izuzetak){
        std::cout<<izuzetak.what()<<std::endl;
    }
	return 0;
}