/*#include <iostream>
int main() {
    std::cout << "Proba 1, Zadatak 1";
    return 0;
}*/
#include <iostream>

int pnk(int n, int k) {
    if(k>n) return 0;
    if(n==0) return 0;
    if(k==1) return 1;
    return pnk(n-1,k-1)+pnk(n-k,k);
}

int main() {
    std::cout << pnk(11,6);
}
